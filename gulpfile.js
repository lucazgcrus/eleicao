var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    return gulp.src('public/view/sass/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('public/view/css'));
});

gulp.task('watch', function() {
    gulp.watch('public/view/sass/*.scss', ['sass']);
})
