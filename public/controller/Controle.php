<?php

session_start();
require_once("model/Usuario.php");
require_once("model/UsuarioFactory.php");
require_once("model/LogFactory.php");
require_once("model/Log.php");

class Controle {

    private $usuarioFactory;
    private $logFactory;

    public function __construct() {
        $this->usuarioFactory = new UsuarioFactory();
        $this->logFactory = new LogFactory();
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
    }

    public function init() {

        if (isset($_GET['funcao'])) {
            $f = $_GET['funcao'];
        } else {
            $f = "";
        }

        switch($f) {
            case 'regioes':
                $this->regioes();
                break;
            case 'mapa':
                $this->mapa();
                break;
            case 'equipes':
                $this->equipes();
                break;
            case 'agentes':
                $this->agentes();
                break;
            case 'questionarios':
                $this->questionarios();
                break;
            case 'relatorios':
                $this->relatorios();
                break;
            case 'acessar':
                $this->acessar($_POST['usuario_email'],$_POST['usuario_senha']);
                break;
            case 'sair':
                $this->sair();
                break;
            default:
                $this->login();
                break;
        }
    }

    public function acessar($email,$senha){
        $usuario = $this->usuarioFactory->acessar($email,$senha);
        if($usuario){
            $_SESSION['usuario'] = $usuario[0]->getId();
            $_SESSION['nome'] = $usuario[0]->getNome();
            $_SESSION['tipo'] = $usuario[0]->getTipo();
            $this->logFactory->salvar(new Log($_SESSION['usuario'], $_SESSION['tipo'], "acessou o sistema", php_uname().' | '.PHP_OS.' | '.$_SERVER['HTTP_USER_AGENT']));
            require 'view/mapa.php';
        }else{
            require 'view/login.php';
        }
    }

    public function sair(){
        session_destroy(); 
        require 'view/login.php';
    }

    public function verificarAcesso(){
        if(isset($_SESSION["usuario"]) && $_SESSION["usuario"]!='') {
            return true;
        }else{
            return false;
        }
    }

    public function login(){
        require 'view/login.php'; 
    }


    public function mapa(){
        if($this->verificarAcesso()){
            require 'view/mapa.php';    
        }else{
            require 'view/login.php';
        }
    }

    public function regioes(){
        if($this->verificarAcesso()){
            require 'view/regioes.php';    
        }else{
            require 'view/login.php';
        }
    }

    public function equipes(){
        if($this->verificarAcesso()){
            require 'view/equipes.php';    
        }else{
            require 'view/login.php';
        }
    }

    public function agentes(){
        if($this->verificarAcesso()){
            require 'view/agentes.php';    
        }else{
            require 'view/login.php';
        }
    }

    public function questionarios(){
        if($this->verificarAcesso()){
            require 'view/questionarios.php';    
        }else{
            require 'view/login.php';
        }
    }

    public function relatorios(){
        if($this->verificarAcesso()){
            require 'view/relatorios.php';    
        }else{
            require 'view/login.php';
        }
    }



    /*
    public function home() {
        require 'view/home.php';
    }

    public function novo() {
        require 'view/novo.php';
    }

    public function cadastra() {
        if (isset($_POST['enviado'])) {

            $nome = $_POST['nome'];
            $email = $_POST['email'];
            $sucesso = false;
            try {
                if ($nome == "" || $email == "")
                    throw new Exception('Erro');

                $contato = new Contato($nome, $email);
                
                //consulta o e-mail no banco
                $result = $this->factory->buscar($email);

                // se o resultado for igual a 0 itens, então salva contato
                if (count($result) == 0) {
                    $sucesso = $this->factory->salvar($contato);
                }

                if ($sucesso) {
                    $msg = "<p>O contato " . $nome . " (" . $email . ") foi cadastrado com sucesso!</p>";
                } else if (!$sucesso && count($result) > 0) {
                    $msg = "<p>O contato n&atilde;o foi adicionado. E-mail j&aacute; existente na agenda!</p>";
                } else {
                    $msg = "<p>O contato n&atilde;o foi adicionado. Tente novamente mais tarde!</p>";
                }

                unset($nome);
                unset($email);

                require_once 'view/mensagem.php';                 
            } catch (Exception $e) {
                if ($nome == "") {
                    $msg = "O campo <strong>Nome</strong> deve ser preenchido!";
                } else if ($email == "") {
                    $msg = "O campo <strong>E-mail</strong> deve ser preenchido!";
                }
                require_once 'view/mensagem.php';
            }
        }
    }

    public function lista() {
        $result = $this->factory->listar();
        require 'view/lista.php';
    }
    /**/
}

?>