<?php

require_once("Usuario.php");
require_once("AbstractFactory.php");

class UsuarioFactory extends AbstractFactory {
    
    private $nomeTabela = 'usuario';
    
    public function acessar($login,$senha){
        try {

            $resultRows = $this->db->prepare("SELECT usu_id, usu_nome, usu_email, usu_tipo, usu_status FROM ".$this->nomeTabela." WHERE usu_email=:login AND usu_senha=:senha AND usu_status='ativo' LIMIT 1");
            $resultRows->bindValue(":login",$login);
            $resultRows->bindValue(":senha",$senha);
            $resultRows->execute();
            
            if(!($resultRows instanceof PDOStatement)){
                throw new Exception("Erro ao executar operação!");
            }
            
            $resultObject = $this->queryRowsToListOfObjects($resultRows,"Usuario");

        } catch (PDOException $exc) {
            echo $exc->getMessage();
            $resultObject = false;
        }
        
        return $resultObject;
    }  

    public function salvar($obj){ }

    public function listar(){ }

    public function buscar($param){ }
}
