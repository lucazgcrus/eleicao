<?php

class Usuario {
    
    private $id;
    private $nome;
    private $email;
    private $tipo;
    private $status;

    public function __construct($id, $nome, $email, $tipo, $status){
        $this->id = $id;
        $this->nome = $nome;
        $this->email = $email;
        $this->tipo = $tipo;
        $this->status = $status;
    }
    
    public function getId(){
        return $this->id;
    }

    public function getNome(){
        return $this->nome;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getTipo(){
        switch ($this->tipo) {
            case 'candidato':
                return "Candidato";
                break;
            case 'comite':
                return "Comitê";
                break;
            case 'gerente':
                return "Gerênte";
                break;
            default:
                return "Inválido";
                break;
        }
        return $this->tipo;
    }

    public function getStatus(){
        return $this->status;
    }
}