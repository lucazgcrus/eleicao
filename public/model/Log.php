<?php

class Log {

    private $log_id;
    private $log_usuario;
    private $log_usuario_tipo;
    private $log_acao;
    private $log_ip;
    private $log_so;
    private $log_data;

    public function __construct($log_usuario, $log_usuario_tipo, $log_acao, $log_so){
        $this->log_usuario = $log_usuario;
        $this->log_usuario_tipo = $log_usuario_tipo;
        $this->log_acao = $log_acao;
        $this->log_so = $log_so;
    }

    public function getLogId(){
        return $this->log_id;
    }

    public function setLogId($log_id){
        $this->log_id = $log_id;
    }

    public function getLogUsuario(){
        return $this->log_usuario;
    }

    public function setLogUsuario($log_usuario){
        $this->log_usuario = $log_usuario;
    }

    public function getLogUsuarioTipo(){
        switch ($this->log_usuario_tipo) {
            case 'Candidato':
                return "candidato";
                break;
            case 'Comitê':
                return "comite";
                break;
            case 'Gerênte':
                return "gerente";
                break;
            default:
                return "Inválido";
                break;
        }
        return $this->log_usuario_tipo;
    }

    public function setLogUsuarioTipo($log_usuario_tipo){
        $this->log_usuario_tipo = $log_usuario_tipo;
    }

    public function getLogAcao(){
        return $this->log_acao;
    }

    public function setLogAcao($log_acao){
        $this->log_acao = $log_acao;
    }

    public function getLogIp(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    public function setLogIp($log_ip){
        $this->log_ip = $log_ip;
    }

    public function getLogSo(){
        return $this->log_so;
    }

    public function setLogSo($log_so){
        $this->log_so = $log_so;
    }

    public function getLogData(){
        return $this->log_data;
    }

    public function setLogData($log_data){
        $this->log_data = $log_data;
    }
}