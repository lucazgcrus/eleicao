<?php

require_once("Log.php");
require_once("AbstractFactory.php");

class LogFactory extends AbstractFactory {
    
    private $nomeTabela = 'log';
    
    public function salvar($obj){
        try {
            
            $resultRows = $this->db->prepare("INSERT INTO ".$this->nomeTabela." (log_usuario, log_usuario_tipo, log_acao, log_ip, log_so) VALUES (:getLogUsuario,:getLogUsuarioTipo,:getLogAcao,:getLogIp,:getLogSo)");
            $resultRows->bindValue(":getLogUsuario",$obj->getLogUsuario());
            $resultRows->bindValue(":getLogUsuarioTipo",$obj->getLogUsuarioTipo());
            $resultRows->bindValue(":getLogAcao",$obj->getLogAcao());
            $resultRows->bindValue(":getLogIp",$obj->getLogIp());
            $resultRows->bindValue(":getLogSo",$obj->getLogSo());

            if($resultRows->execute()){
                $result = true;
            }else{
                $result = false;
            }
        } catch (PDOException $exc) {
            echo $exc->getMessage();
            $result = false;
        }        
        return $result;
    }

    public function listar(){ }

    public function buscar($param){ }
}
