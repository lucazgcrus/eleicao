<!DOCTYPE html>
<html>
<head>
	<title>Eleição</title>
	<link rel="stylesheet" type="text/css" href="view/css/app.css">
    <link rel="stylesheet" type="text/css" href="view/css/login.css">
</head>
<body>
    <div class="login-area">
        <div class="left-content bg-blue">
			<div class="content-login">
				<img src="view/images/logo-login.png" alt="Eleição">
				<h1>Bem-Vindo!</h1>
				<p>Gerencie seus agentes de campo de uma maneira totalmente intuitiva e amigável. Descubra onde estão, e como está o andamento das pesquisas.</p>
			</div>
		</div>
        <div class="right-content">
			<h1>Entrar</h1>
			<div class="inputs">
				<form class="login-div" action="?funcao=acessar" method="post">
					<input type="text" class="input-style-login" name="usuario_email" placeholder="E-mail">
					<input type="password" class="input-style-login" name="usuario_senha" placeholder="Senha">
					<div class="action-area">
						<a class="reset-password" href="#">Esqueci minha senha</a>
						<button type="button" class="btn-login" name="button" onmouseup="$('.login-div').submit();">Entrar</button>
					</div>
				</form>
				<div class="recovery-password">
					<input type="email" class="input-style-login" name="" placeholder="E-mail de Recuperação">
					<div class="action-area">
						<a class="return-login-area" href="#">VOLTAR</a>
						<button type="button" class="btn-login btn-reset-password" name="button">Enviar</button>
					</div>
				</div>
			</div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="view/js/login.js" type="text/javascript"></script>
</body>
</html>
