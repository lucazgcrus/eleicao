<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Eleição</title>
		<link rel="stylesheet" type="text/css" href="view/css/app.css">
		<?php
			if($pagina=='regioes'){
				?><link rel="stylesheet" href="view/css/regioes.css"><?php
			}elseif($pagina=='equipes'){
				?><link rel="stylesheet" href="view/css/equipes.css"><?php
				?><link rel="stylesheet" href="view/css/easydropdown.flat.css"><?php
			}elseif($pagina=='mapa'){
				?><link rel="stylesheet" href="view/css/map.css"><?php
			}
			elseif($pagina=='agentes'){
				?><link rel="stylesheet" href="view/css/agentes.css"><?php
			}
			elseif($pagina=='questionarios'){
				?><link rel="stylesheet" href="view/css/agentes.css"><?php
			}
			elseif($pagina=='relatorios'){
				?><link rel="stylesheet" href="view/css/agentes.css"><?php
			}
		?>
	</head>

	<body class="bg-gray">
		<div class="navbar<?php if($pagina=='regioes'){ ?> no-absolute<?php } ?>">
			<div class="left-content">
				<div class="logo-black"></div>
				<div class="divisor-vertical"></div>
				<div class="user-name">
					<p><?php echo $_SESSION['nome']; ?></p>
					<span class="tiny-text"><?php echo $_SESSION['tipo']; ?></span>
				</div>
			</div>
			<div class="center-content">
				<a class="link-menu-navbar" href="?funcao=mapa">Mapa</a>
				<a class="link-menu-navbar" href="?funcao=regioes">Regiões</a>
				<a class="link-menu-navbar" href="?funcao=equipes">Equipes</a>
				<a class="link-menu-navbar" href="?funcao=agentes">Agentes</a>
				<a class="link-menu-navbar" href="?funcao=questionarios">Questionários</a>
				<a class="link-menu-navbar" href="?funcao=relatorios">Relatórios</a>
			</div>
			<div class="right-content">
				<a href="?funcao=sair">
					<div class="icon-exit"></div>
				</a>
			</div>
		</div>
