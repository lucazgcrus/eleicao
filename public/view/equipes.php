<?php
$pagina = "equipes";
include_once 'navbar.php';
?>

		<div class="container">
			<div class="sub-nav">
				<div class="breadcrumb">
					<h2 class="title-page">Nova Equipe</h2>
					<span>Início  /  <b>Equipes</b></span>
				</div>

				<select class="dropdown">
					<option class="label">Filtrar</option>
					<option>Por Questionário</option>
					<option>Por Região</option>
				</select>
			</div>
			<div class="content">
				<div class="teams">
					<div class="box-team">
						<span>Time A</span>
						<h2 class="n-agentes">32 Agentes</h2>
						<a href="#" class="b-action bt-primary-text">VER AGENTES</a>
					</div>
					<div class="box-team">
						<span>Time B</span>
						<h2 class="n-agentes">32 Agentes</h2>
						<a href="#" class="b-action bt-primary-text">VER AGENTES</a>
					</div>
					<div class="box-team">
						<span>Time C</span>
						<h2 class="n-agentes">32 Agentes</h2>
						<a href="#" class="b-action bt-primary-text">VER AGENTES</a>
					</div>
					<div class="box-team">
						<span>Time D</span>
						<h2 class="n-agentes">32 Agentes</h2>
						<a href="#" class="b-action bt-primary-text">VER AGENTES</a>
					</div>
					<div class="box-team">
						<span>Time E</span>
						<h2 class="n-agentes">32 Agentes</h2>
						<a href="#" class="b-action bt-primary-text">VER AGENTES</a>
					</div>
					<div class="box-team">
						<span>Time F</span>
						<h2 class="n-agentes">32 Agentes</h2>
						<a href="#" class="b-action bt-primary-text">VER AGENTES</a>
					</div>
					<div class="box-team">
						<span>Time G</span>
						<h2 class="n-agentes">32 Agentes</h2>
						<a href="#" class="b-action bt-primary-text">VER AGENTES</a>
					</div>
                </div>
			</div>
		</div>

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="js/jquery.easydropdown.js" type="text/javascript"></script>
    </body>
</html>
