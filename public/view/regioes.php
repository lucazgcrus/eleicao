<?php
	$pagina = "regioes";
	include_once 'navbar.php';
?>
<link rel="stylesheet" href="view/css/regioes.css">
	<div class="drawer">
		<div class="header-drawer">
			<a href="#" class="icon-right-arrow"></a>
			<span class="normal-text">Cadastrar Equipes</span>
		</div>
		<div class="content-drawer">
			<h2 class="title-drawer">Região - <b class="bold-text">Moreninhas</b></h2>

			<div class="group-drawer">
				<div class="input-group-drawer m-t-30 m-b-10">
					<label class="tiny-text">Nome da Equipe</label>
					<input type="text" class="input-style">
				</div>
				<button class="bt bt-primary-outline bt-tiny">Inserir Agentes</button>
			</div>
			<div class="group-drawer">
				<div class="input-group-drawer m-t-30 m-b-10">
					<label class="tiny-text">Nome da Equipe</label>
					<input type="text" class="input-style">
				</div>
				<button class="bt bt-primary-outline bt-tiny"><div class="icon-add"></div></button>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="sub-nav">
			<div class="breadcrumb">
				<h2 class="title-page">Nova Região</h2>
				<span>Início  /  <b>Região</b></span>
			</div>
		</div>
		<div class="content">
			<div class="center-content">
				<i class="icon-no-region"></i>
				<span>Nenhuma região cadastrada</span>
				<button  id="register-region" type="button" class="bt bt-primary" name="button">CADASTRAR REGIÃO</button>
			</div>
			<div class="group">
				<div class="input-group">
					<label class="tiny-text">Região</label>
					<select class="input-style" name="">
						<option>Moreninhas</option>
						<option>Copa Vila</option>
						<option>Santo Eugênio</option>
					</select>
				</div>
				<div class="action-input m-t-16">
					<button class="bt bt-primary-outline criar-equipe" type="button" name="button">Criar Equipe</button>
				</div>
			</div>
		</div>
	</div>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="view/js/regioes.js" type="text/javascript"></script>
	</body>
</html>
