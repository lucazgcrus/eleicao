var map;
$(document).ready(function(){
  map = new GMaps({
    el: '#map',
    lat: -12.043333,
    lng: -77.028333,
  });
  var path = [
        [-12.040397656836609,-77.03373871559225],
        [-12.040248585302038,-77.03993927003302],
        [-12.050047116528843,-77.02448169303511],
        [-12.044804866577001,-77.02154422636042]
      ];

  polygon = map.drawPolygon({
    paths: path,
    strokeColor: '#BBD8E9',
    strokeOpacity: 1,
    strokeWeight: 3,
    fillColor: '#BBD8E9',
    fillOpacity: 0.6
  });

  map.addMarker({
    lat: -12.043333,
    lng: -77.028333,
    icon: 'view/images/icon-agent.png',
    fences: [polygon],
	infoWindow: {
	  content:
	  	'<div id="tooltip-pin">' +
			'<div class="info-pin">' +
		  		'<span class="tiny-text">Agente</span>' +
				'<p class="normal-text">Rodrigo Golfeto Queiroz</p>' +
			'</div>' +
			'<div class="info-pin">' +
		  		'<span class="tiny-text">Localização</span>' +
				'<p class="normal-text">R. Afonso Pena, 1000 - Centro</p>' +
			'</div>' +
			'<div class="info-pin">' +
		  		'<span class="tiny-text">Pesquisas</span>' +
				'<p class="normal-text">124</p>' +
			'</div>' +
			'<div class="footer-pin">' +
				'<a href="#" class="icon-audio">' +
				'<a href="#" class="icon-chat">' +
				'<a href="#" class="icon-camera">' +
			'</div>' +
		'</div>'
	}
  });
  map.addMarker({
    lat: -12.044333,
    lng: -77.023333,
    icon: 'view/images/icon-agent.png'
  });
  map.addMarker({
    lat: -12.049333,
    lng: -77.015333,
    icon: 'view/images/icon-agent.png',
    fences: [polygon],
  });
  map.addMarker({
    lat: -12.024333,
    lng: -77.103333,
    icon: 'view/images/icon-agent.png'
  });
});
