-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           10.1.25-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela eleicao.agente
CREATE TABLE IF NOT EXISTS `agente` (
  `age_id` int(11) NOT NULL AUTO_INCREMENT,
  `age_usu_id` int(11) NOT NULL DEFAULT '0',
  `age_nome` varchar(100) NOT NULL,
  `age_email` varchar(100) NOT NULL,
  `age_senha` varchar(100) NOT NULL,
  `age_status` enum('ativo','inativo') NOT NULL DEFAULT 'inativo',
  `age_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`age_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela eleicao.agente: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `agente` DISABLE KEYS */;
INSERT INTO `agente` (`age_id`, `age_usu_id`, `age_nome`, `age_email`, `age_senha`, `age_status`, `age_data`) VALUES
	(1, 2, 'João da Silva', 'joao@gmail.com', '123', 'ativo', '2017-12-08 23:24:52');
/*!40000 ALTER TABLE `agente` ENABLE KEYS */;

-- Copiando estrutura para tabela eleicao.log
CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_usuario` int(11) DEFAULT '0',
  `log_usuario_tipo` enum('candidato','comite','gerente','agente') DEFAULT NULL,
  `log_acao` varchar(50) DEFAULT NULL,
  `log_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela eleicao.log: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Copiando estrutura para tabela eleicao.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_nome` varchar(100) NOT NULL,
  `usu_email` varchar(100) NOT NULL,
  `usu_senha` varchar(100) NOT NULL,
  `usu_tipo` enum('candidato','comite','gerente') NOT NULL DEFAULT 'gerente',
  `usu_status` enum('ativo','inativo') NOT NULL DEFAULT 'inativo',
  `usu_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela eleicao.usuario: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`usu_id`, `usu_nome`, `usu_email`, `usu_senha`, `usu_tipo`, `usu_status`, `usu_data`) VALUES
	(1, 'Gustavo Godoy', 'g@gmail.com', '123', 'candidato', 'ativo', '2016-12-08 23:03:56'),
	(2, 'Rodrigo Golfeto', 'r@gmail.com', '123', 'gerente', 'ativo', '2017-12-08 23:05:10'),
	(3, 'Lucas Cruz', 'l@gmai.com', '123', 'gerente', 'ativo', '2017-12-08 23:05:38'),
	(4, 'Maria Silva', 'm@gmail.com', '123', 'comite', 'ativo', '2017-12-08 23:06:57');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
